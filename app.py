from flask import Flask, request, Response, make_response
import json
import uuid
app = Flask(__name__)
 
@app.route("/authenticate", methods = ['POST'])
def authenticate():
    content = request.get_json()

    # is user authorized ?
    # do not check password
    if not request.authorization:
        return Response(
            'Could not verify your access level for that URL.\n'
            'You have to login with proper credentials', 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        )

    if request.authorization['username'] != 'admin':
        return Response(
            'Could not verify your access level for that URL.\n'
            'You have to login with proper credentials', 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        )

    data = {
        "id_token": "{}".format(uuid.uuid4()),
    }
    response = make_response(json.dumps(data))
    response.headers["content-type"] = "application/json"
    return response
 
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
